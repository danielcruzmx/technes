# Uso -> Docker build -t python373django22 .

FROM python:3.7.3-slim
COPY . /home
WORKDIR /home
RUN pip install --no-cache-dir -r requirements.txt

