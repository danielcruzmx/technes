Django==2.2
django-sql-explorer==1.1.2
whitenoise==4.1.2
gunicorn==19.9.0
django-otp==0.6.0
qrcode==6.1
