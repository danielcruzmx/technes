from django.contrib import admin

# Register your models here.

from .models import Estado, Trabajo, Operador, Orden, DetalleOrden

class DetalleOrdenInline(admin.TabularInline):
    model = DetalleOrden
    fields = ['operador','trabajo','estado','fecha_inicio','fecha_fin']
    
    def get_extra(self, request, obj=None, **kwargs):
        extra = 0
        return extra
    
class DetalleOperadorInline(admin.TabularInline):
    model = DetalleOrden
    fields = ['orden','trabajo','estado','fecha_inicio','fecha_fin']

    def get_extra(self, request, obj=None, **kwargs):
        extra = 0
        return extra
      
@admin.register(Estado)
class EstadoAdmin(admin.ModelAdmin):
    list_display = ('id','descripcion',)

@admin.register(Trabajo)
class TrabajoAdmin(admin.ModelAdmin):
    list_display = ('id','descripcion',)

@admin.register(Operador)
class OperadorAdmin(admin.ModelAdmin):
    list_display = ('id','operador','nombre','linea',)
    inlines = [DetalleOperadorInline]

@admin.register(Orden)
class OrdenAdmin(admin.ModelAdmin):
    list_display = ('numero','producto','descripcion','cliente',)
    change_list_template = "admin/orden.html"
    inlines = [DetalleOrdenInline]
