from django.db import models

# Create your models here.

class Estado(models.Model):
    descripcion = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.descripcion)

    class Meta:
        managed = True
        db_table = 'estado_trabajo'
        verbose_name_plural = "Catálogo estados"

class Trabajo(models.Model):
    descripcion = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return '%s' % (self.descripcion)

    class Meta:
        managed = True
        db_table = 'tipo_trabajo'
        verbose_name_plural = "Tipos de trabajo"
        
class Operador(models.Model):
    operador = models.DecimalField(max_digits=10, decimal_places=0, default=0)
    nombre = models.CharField(max_length=80)
    linea = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return '%d %s %s' % (self.operador, self.nombre, self.linea)

    class Meta:
        managed = True
        db_table = 'operador'
        verbose_name_plural = "Operadores"

class Orden(models.Model):
    numero = models.DecimalField(max_digits=10, decimal_places=0, default=0)
    producto = models.CharField(max_length=25, blank=True, null=True)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    observaciones = models.CharField(max_length=100, blank=True, null=True)
    cantidad = models.DecimalField(max_digits=4, decimal_places=0, default=0)
    cliente = models.CharField(max_length=40, blank=True, null=True)
    fecha_inicio = models.DateTimeField(blank=True, null=True)
    fecha_fin = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '%s %s %s %s' % (self.numero, self.producto, self.descripcion, self.cliente)
    
    class Meta:
        managed = True
        db_table = 'gporeq_orden'
        ordering = ['numero']
        verbose_name_plural = "Ordenes"

class DetalleOrden(models.Model):
    orden = models.ForeignKey(Orden, verbose_name = ('Orden'), on_delete = models.CASCADE)
    operador = models.ForeignKey(Operador, verbose_name = ('Operador'), on_delete = models.CASCADE)
    trabajo = models.ForeignKey(Trabajo, verbose_name = ('Trabajo'), on_delete = models.CASCADE)    
    estado = models.ForeignKey(Estado, verbose_name = ('Estado'), on_delete = models.CASCADE)
    fecha_inicio = models.DateTimeField(blank=True, null=True)
    fecha_fin = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '%s %s %s %s' % (self.orden, self.operador, self.trabajo, self.estado)

    class Meta:
        managed = True
        db_table = 'orden_detalle_operador'
        ordering = ['orden']    
        verbose_name_plural = "Detalle Orden"
