from django.shortcuts               import render
from django.http                    import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .models                        import Orden, DetalleOrden, Operador, Trabajo, Estado
from .forms                         import OrdenForm
from django.utils.safestring        import mark_safe
from django.contrib.auth            import logout


import csv
import os
import datetime

from django.conf import settings

@login_required()
def logo(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required()
def home(request):
    return render(request, 'inicio.html', {})

@login_required()
def get_carga(request):
    archivo_a_bd('datos.csv')
    return render(request, 'inicio.html', {})

@login_required()
def get_orden(request):
    if request.method == 'POST':
        form = OrdenForm(request.POST)
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            numorden = form.cleaned_data['orden']    
            '''print(numorden)
            with open(os.path.join(settings.BASE_DIR, 'datos.csv'), encoding='latin1') as archivo_csv:
                dict_lector = csv.DictReader(archivo_csv, delimiter=';')
                campos = dict_lector.fieldnames
                f_file = False
                f_base = False
                for linea in dict_lector:
                    valor = linea['OP'].split('/')
                    if valor[1]==numorden:
                        f_file = True 
                        print('encontrado en archivo')    
                        break
                try:
                    orden = Orden.objects.get(numero=numorden)
                    print('encontrado en BD')
                    f_base = True 
                except Orden.DoesNotExist:
                    print('no encontrado en BD')    
                    if f_file:
                        print('agregar a base de datos')    
                        for x in linea:
                            print(x,'->', linea[x])
                        agrega_orden(linea)    
                        fecha =  datetime.datetime.strptime(linea['VIGENTE DESDE'],"%d/%m/%Y").date()    
                        n_orden = Orden(numero        = numorden,\
                                        producto      = linea['PRODUCTO'],\
                                        descripcion   = linea['NOMBRE'],\
                                        observaciones = linea['OBSERVACIONES'],\
                                        cantidad      = linea['CANTIDAD'],\
                                        cliente       = linea['CLIENTE'],\
                                        fecha_inicio  = fecha,\
                                        fecha_fin     = fecha)
                        n_orden.save()
                        f_base = True
                if f_base:'''
            # muestraregistro 
            id_orden = None
            try:
                id_orden = Orden.objects.get(numero=numorden).id
            except Orden.DoesNotExist:
                print('error inesperado')  
                if id_orden is not None:            
                    m_url = '/admin/gporeq/orden/%s/' % (id_orden)
                    print(m_url)
                    return HttpResponseRedirect(m_url)
                else:
                    return HttpResponseRedirect('/')    
            else:
                print('No existe registro en base de datos')
                return HttpResponseRedirect('/')
    else:
        form = OrdenForm()

    return render(request, 'orden.html', {'form': form})

def archivo_a_bd(archivo):
    with open(os.path.join(settings.BASE_DIR, archivo), encoding='latin1') as archivo_csv:
        dict_lector = csv.DictReader(archivo_csv, delimiter=';')
        campos = dict_lector.fieldnames
        for linea in dict_lector:
            valor = linea['OP'].split('/')
            if busca_orden(valor[1]):
                print(" En BD ")
            else:
                print(" no en BD ")
                agrega_orden(linea)

def agrega_orden(linea):
    numorden = linea['OP'].split('/')
    fecha =  datetime.datetime.strptime(linea['VIGENTE DESDE'],"%Y-%m-%d %H:%M:%S").date()    
    n_orden = Orden(numero        = numorden[1],\
                    producto      = linea['PRODUCTO'],\
                    descripcion   = linea['NOMBRE'],\
                    observaciones = linea['OBSERVACIONES'],\
                    cantidad      = linea['CANTIDAD'],\
                    cliente       = linea['CLIENTE'],\
                    fecha_inicio  = fecha,\
                    fecha_fin     = fecha)
    n_orden.save() 
    #print(n_orden.id)   
    d_orden = n_orden
    d_operador = Operador.objects.get(id = 1)
    d_estado = Estado.objects.get(id = 1)
    id_trabajo = [0,0,0,0]
    print(linea)
    if linea['TAPIZADO'] == 'Si':
        id_trabajo[0] = 1
    if linea['COSTURA'] == 'Si':
        id_trabajo[1] = 2
    if linea['EMPACADO'] == 'Si':
        id_trabajo[2] = 3
    if linea['ARMADO'] == 'Si':
        id_trabajo[3] = 4
    for t in id_trabajo:
        #print(d_orden,t)
        if t:
            d_trabajo = Trabajo.objects.get(id = t)
            n_detalle = DetalleOrden(orden = d_orden, \
                                     operador = d_operador, \
                                     trabajo = d_trabajo, \
                                     estado = d_estado,
                                     fecha_inicio = fecha, \
                                     fecha_fin = fecha)
            n_detalle.save()        

def busca_orden(numorden):
    f_base = None
    try:
        orden = Orden.objects.get(numero=numorden)
        print('encontrado en BD')
        f_base = orden 
    except Orden.DoesNotExist:
        print('no encontrado en BD')    
    return f_base
