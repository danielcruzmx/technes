"""technes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from gporeq import views
from django.contrib.auth.views import LoginView, LogoutView
from django.utils.functional import curry
from django.views.defaults import server_error, page_not_found

admin.site.site_header = "Grupo Requiez"
admin.site.site_title  = "Technes"
admin.site.index_title = "Control de Produccion"

handler500 = curry(server_error, template_name='admin/500.html')
handler404 = curry(page_not_found, template_name='admin/404.html')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('orden/', views.get_orden),
    path('carga/', views.get_carga),
    path('logout/', views.logo),
    path('accounts/login/', LoginView.as_view(template_name='admin/login.html'), name="login"),
    url(r'^$', views.home),
]
